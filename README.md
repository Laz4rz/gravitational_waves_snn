# Klasyfikacja sygnałów z detektorów fal grawitacyjnych z wykorzystaniem sieci pulsujących (SNN)
### Plan pracy
1. Przerobienie tutoriali snnTorch
2. Implementacja prostej sieci konwolucyjnej dla zestawu MNIST
3. Przełożenie prostej sieci konwolucyjnej ANN na SNN
4. Przełożenie udostępnionego na Kaggle modelu na SNN
5. Testowanie parametrów 
6. Opisanie pracy
